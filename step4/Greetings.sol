pragma solidity ^0.4.24;

contract Greetings{
    string public message;

    constructor() public {
        message = "Welcome!";
    }

    function setMessage(string newMessage) public {
        message = newMessage;
    }
}
